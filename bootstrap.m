%
% Bootstrap UFTIR
%

uftir_tr

t = c0(2) + c(2)*(data(:,1) - t0(2));
tU = c0U(2) + cU(2)*(data(:,1) - t0(2));
tL = c0L(2) + cL(2)*(data(:,1) - t0(2));

echo, 'a + b*t'
echo, 'a ='
c0(2)
echo, 'b ='
c(2)
echo off

plot(data(:,1),data(:,2))
hold
plot(data(:,1),t)
plot(data(:,1),tL)
plot(data(:,1),tU)
%a(2*nf,1)             Fourier coefficients. Fourier series is 
%                         a(1)*cos(2*pi*t) +
%                         a(2)*sin(2*pi*t) +
%                         a(3)*cos(4*pi*t) + 
%                         ...
%                         a(2*nf)*sin(2*nf*pi*t)

time=data(:,1) - t0(2);

nfit = nf;
fit = t;
freq = 1;

for i=1:2*nfit;

    if mod(i,2) == 0;
    
        fit = fit + a(i,2)*sin(2*freq*pi*time);
        freq = freq + 1;
    else
    
        fit = fit + a(i,2)*cos(2*freq*pi*time);
       
    end
end


%plot(data(:,1),t + a(1,2)*cos(2*pi*time)  + a(2,2)*sin(2*pi*time) + a(3,2)*cos(4*pi*time) + a(4,2)*sin(4*pi*time),'r' )

%plot(data(:,1),t + a(1,2)*cos(2*pi*time)  + a(2,2)*sin(2*pi*time) + a(3,2)*cos(4*pi*time) + a(4,2)*sin(8*pi*time),'r' )

%fit = t + a(1,2)*cos(2*pi*time)  + a(2,2)*sin(2*pi*time) + a(3,2)*cos(4*pi*time) + a(4,2)*sin(8*pi*time) + a(5,2)*cos(10*pi*time)

%hold off

plot(data(:,1),fit,'r' )

residum = data(:,2) .\ fit;
%hold off
%plot(data(:,1),residum)


%save results
[filepath,name,ext] = fileparts(df)
%save plot
saveas(gcf,strcat(dp,'result_',name,'.fig'))
saveas(gcf,strcat(dp,'result_',name,'.jpg'))
%Write results to file
fileID = fopen(strcat(dp,'result_',df),'w');
for i=1:numel(t);
    fprintf(fileID,'%20.10f %20.10e %20.10e %20.10e\n',data(i,1),data(i,2),fit(i),t(i));
end
fclose(fileID);
%save variables
save(strcat(dp,'result_',name,'.mat'))

hold off