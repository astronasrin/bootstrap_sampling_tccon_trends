function [t0,c0,c,c0L,c0U,cL,cU,Uc,a,res, fname] = bs0_driftfourier(nf,t,y,N,fname)
% -------------------------------------------------------------------------
% BS0_DRIFTFOURIER.M     Easy to use version of BS_DRIFTFOURIER.M
%                        Fit drift + fourier to data (t,y). 
%                        Determine bootstrap estimates of drift parameters
% 
% v11 2005-05-16         ABF
% Author                 A B Forbes, NPL. (c) Crown Copyright
% -------------------------------------------------------------------------
% Input
%  nf                    degree of fourier series
%  t(m,1)                time in YEARS
%  y(m,1)                measurements
%  N                     number of bootstrap replicates
%
% Output
%  (t0,c0)               point on drift line
%  c                     slope
% Note: drift line is 
%
%          d = c0 + c*(t-t0)
%
%  c0L                   95 percent lower limit for intercept c0
%  c0U                   95 percent upper limit for intercept c0
%  cL                    95 percent lower limit for slope c
%  cU                    95 percent upper limit for slope c
%  Uc(2,2)               uncertainty matrix for drift parameters
%                        c0 and c calculated from bootstrap replicates
% Note: the uncertainty u(d(t)) of the drift line at t is estimated by
% 
%         u^2(d(t)) = [ 1 (t-t0) ]*Uc*[1; (t-t0)].
%
%  a(2*nf,1)             Fourier coefficients. Fourier series is 
%                         a(1)*cos(2*pi*t) +
%                         a(2)*sin(2*pi*t) +
%                         a(3)*cos(2*pi*t) + 
%                         ...
%                         a(2*nf)*sin(2*pi*t)
% -------------------------------------------------------------------------
% [t0,c0,c,c0L,c0U,cL,cU,Uc,a] = bs0_driftfourier(nf,t,y,N) 
% -------------------------------------------------------------------------
  [alpha,sigmah,ualpha,Ualpha,alphaBS,ualphaBS,UBS,CBS,c0L,c0U,cL,cU, fname] = ...
        bs_driftfourier_V13(nf,0.5,t,y,N,fname);

  t0 = min(t);
  c0 = alpha(1);
  c  = alpha(2);
  a  = alpha(3:2*nf+2);
    
  Uc = UBS(1:2,1:2);
  res = CBS(:,2);