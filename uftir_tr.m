% -------------------------------------------------------------------------
% uftir_tr.m
%     
% -------------------------------------------------------------------------
% Front-end Matlab script file to run bootstrap resampling trend analysis. 
% This version developed for analysis of UFTIR data sets.
%
% -------------------------------------------------------------------------
% Input data format = [m rows by n columns] ascii file
%   Where :
%           m is the number of measurements in the datasets
%           the first column is the time of each measurements (in
%           fractional years, eg 2000.216 for 20/03/2000)
%           columns 2 to n are the datasets to be analysed for trends (for
%           example each layer of the measured profiles)
% 
% -------------------------------------------------------------------------
% 
% The script then calls a series of functions developed by A. Forbes at NPL 
%
% These fit a trend + fourier series to the data and determine bootstrap
% estimates of the trend parameters and their uncertainties
%
% -------------------------------------------------------------------------
% Initial parameters :
%  data                  input data file 
%  dp                    data file path (string)
%  df                    data file name (string)
%  n                     number of bootstrap replicates
%  nf                    degree of fourier series
%
% Output parameters :
%  (t0,c0)               initial point on trend line
%  c                     trend line slope (ie annual trend value)
%
% Note that the trend line is given by : 
%          Trend = c0 + c*(time-t0)
%
%  c0L                   95 percent lower limit for intercept c0
%  c0U                   95 percent upper limit for intercept c0
%  cL                    95 percent lower limit for slope c 
%                           (ie -ve trend uncertainty)
%  cU                    95 percent upper limit for slope c
%                           (ie +ve trend uncertainty)
%
%  a(2*nf,1)             Fourier coefficients. Fourier series is 
%                         a(1)*cos(2*pi*t) +
%                         a(2)*sin(2*pi*t) +
%                         a(3)*cos(4*pi*t) + 
%                         ...
%                         a(2*nf)*sin(2*nf*pi*t)
%
% Note that the first column of all the above output parameters is zero in order
% to match the number of columns with the input data.
%
% ------------------------------------------------------------------------- 
% v1 2006-01-10          TDG
% Author                 T Gardiner, NPL. (c) Crown Copyright
% -------------------------------------------------------------------------
  clear
  help uftir_tr
 
% select and load input data file  
[df,dp]=uigetfile('*.*','ASCII data file');

filepath=[dp,df];
 
data_file=df
  %format long e
data=importdata(filepath);
%load, filepath; 
 %data=input
[filepath,name,ext] = fileparts(df)
fname = name
% set number of resamples and fourier series order
% default values are 5000 resamples and series order of 3

%  n = 4327; % Nur f�r Jungfraujoch
%n = 5000;
n=5000
number_of_resamples=n
  
  nf = 4;
Fourier_series_order=nf
  

% run trend analysis for each of the data columns in the input matrix

  [pts,cols]=size(data);
number_of_datasets = cols-1
  for i=2:cols
  
  format long e
 [t0(i),c0(i),c(i),c0L(i),c0U(i),cL(i),cU(i),Uc,a(:,i),resamp(:,i), name] = bs0_driftfourier(nf,data(:,1),data(:,i),n,fname);
 
  end
 
 clear number_of_resamples number_of_datasets Fourier_series_order filepath data_file pts cols Uc i resamp
